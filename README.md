aehostd -- Custom NSS/PAM demon
===============================

[aehostd](https://www.ae-dir.com/aehostd.html) is a custom NSS/PAM demon 
specifically designed to be used with [Æ-DIR](https://ae-dir.com).

It is of no general use and cannot be used with other LDAP servers.
