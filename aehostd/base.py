# -*- coding: utf-8 -*-
"""
aehostd.base - very basic stuff
"""

import os
import logging


def dict_del(dct, key):
    """
    removes a dictionary element given by `key' but without failing if it
    does not exist
    """
    try:
        del dct[key]
    except KeyError:
        pass


class IdempotentFile:
    """
    Class handles a idempotent file on disk
    """

    def __init__(self, path):
        self.path = path

    def __repr__(self):
        return '%s.%s(%r)' % (self.__class__.__module__, self.__class__.__name__, self.path)

    def read(self):
        """
        reads content from file
        """
        try:
            with open(self.path, 'rb') as fileobj:
                content = fileobj.read()
        except Exception as err:
            content = None
            logging.warning('Error reading file %r: %s', self.path, err)
        return content # end of IdempotentFile.read()

    def write(self, content, remove=False, mode=None):
        """
        writes content to file if needed
        """
        exists = os.path.exists(self.path)
        if exists and content == self.read():
            logging.debug(
                'Content of %r (%d bytes) did not change => skip updating',
                self.path,
                len(content),
            )
            return False
        # if requested remove old file
        if exists and remove:
            try:
                os.remove(self.path)
            except OSError:
                pass
        # actually write new content to file
        try:
            with open(self.path, 'wb') as fileobj:
                fileobj.write(content)
            if mode is not None:
                os.chmod(self.path, mode)
        except Exception as err:
            updated = False
            logging.error(
                'Error writing content to file %r: %s',
                self.path,
                err,
            )
        else:
            updated = True
            logging.info('Wrote new content (%d bytes) to file %r', len(content), self.path)
        return updated # end of IdempotentFile.write()
