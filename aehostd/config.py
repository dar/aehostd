# -*- coding: utf-8 -*-
"""
aehostd.config - routines for getting configuration information
"""

from .cfg import CFG
from . import req


CONFIG_REQ_GET = 0x00010001

CONFIG_PASSWORD_PROHIBIT_MESSAGE = 1


class ConfigGetRequest(req.Request):
    """
    handle password change requests (mainly denying them)
    """

    rtype = CONFIG_REQ_GET

    def _read_params(self) -> dict:
        return dict(cfgopt=self.tios.read_int32())

    def write(self, value):
        self.tios.write_int32(req.RES_BEGIN)
        self.tios.write_string(value)
        self.tios.write_int32(req.RES_END)

    def process(self):
        """
        reject the password change request
        """
        cfgopt = self._params['cfgopt']
        if cfgopt == CONFIG_PASSWORD_PROHIBIT_MESSAGE:
            self.write(CFG.pam_passmod_deny_msg or '')
        else:
            # return empty response
            self.tios.write_int32(req.RES_END)
