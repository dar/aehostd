# -*- coding: utf-8 -*-
"""
aehostd.pam - PAM authentication, authorisation and session handling
"""

import time
import hashlib
import logging
import socket
import threading
import secrets

import ldap0
from ldap0.controls.ppolicy import PasswordPolicyControl
import ldap0.filter
from ldap0.controls.sessiontrack import SessionTrackingControl, SESSION_TRACKING_FORMAT_OID_USERNAME
from ldap0.pw import random_string

import aedir

from .cfg import CFG
from . import req
from .passwd import PASSWD_NAME_MAP
from .ldapconn import LDAP_CONN
from . import refresh

# PAM request type codes
PAM_REQ_AUTHC = 0x000d0001
PAM_REQ_AUTHZ = 0x000d0002
PAM_REQ_SESS_O = 0x000d0003
PAM_REQ_SESS_C = 0x000d0004
PAM_REQ_PWMOD = 0x000d0005

# PAM result constants
PAM_SUCCESS = 0
PAM_PERM_DENIED = 6
PAM_AUTH_ERR = 7
PAM_CRED_INSUFFICIENT = 8
PAM_AUTHINFO_UNAVAIL = 9
PAM_USER_UNKNOWN = 10
PAM_MAXTRIES = 11
PAM_NEW_AUTHTOK_REQD = 12
PAM_ACCT_EXPIRED = 13
PAM_SESSION_ERR = 14
PAM_AUTHTOK_ERR = 20
PAM_AUTHTOK_DISABLE_AGING = 23
PAM_IGNORE = 25
PAM_ABORT = 26
PAM_AUTHTOK_EXPIRED = 27

# for generating session IDs
SESSION_ID_LENGTH = 25
SESSION_ID_ALPHABET = (
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    "abcdefghijklmnopqrstuvwxyz"
    "01234567890"
)

# random bytes generated at startup
AUTHC_CACHE_SALT = secrets.token_bytes(16)

# dictionary for caching PAMAuthcReq.process() results
# define before importing .refresh sub-module
AUTHC_CACHE = {}


class AuthcCachePurgeThread(threading.Thread):
    """
    Thread for purging expired entries from global AUTHC_CACHE
    """
    schedule_interval = 0.4

    def __init__(self, purge_interval):
        threading.Thread.__init__(
            self,
            group=None,
            target=None,
            name=None,
            args=(),
            kwargs={}
        )
        self.purge_interval = purge_interval
        self.enabled = True
        self._next_run = time.time()

    @staticmethod
    def _purge_expired(current_time):
        # first just collect cache entries to be removed
        del_cache_keys = []
        for cache_key, val in AUTHC_CACHE.items():
            _, _, _, etime = val
            if etime < current_time:
                logging.debug('Cached PAM authc result expired => will remove it')
                del_cache_keys.append(cache_key)
        logging.debug('Found %d expired PAM authc results', len(del_cache_keys))
        # now really remove the keys
        expired = 0
        for cache_key in del_cache_keys:
            try:
                del AUTHC_CACHE[cache_key]
            except KeyError:
                pass
            else:
                expired += 1
        if expired:
            logging.info('Expired %d PAM authc result from cache', expired)
        # end of _purge_expired()

    def run(self):
        """
        retrieve data forever
        """
        logging.debug('Starting %s.run()', self.__class__.__name__)
        while self.enabled:
            current_time = time.time()
            if current_time > self._next_run:
                logging.debug('Invoking %s._purge_expired()', self.__class__.__name__)
                self._purge_expired(current_time)
                self._last_run = current_time
                self._next_run = current_time + self.purge_interval
            time.sleep(self.schedule_interval)
        logging.debug('Exiting %s.run()', self.__class__.__name__)


class PAMRequest(req.Request):
    """
    base class for handling PAM requests (not directly used)
    """

    def _get_rhost(self, params):
        if 'rhost' in params and params['rhost']:
            return params['rhost']
        peer_env = self._get_peer_env(names=('SSH_CLIENT',))
        return peer_env.get('SSH_CLIENT', '').split(' ')[0]

    def _session_tracking_control(self):
        """
        return SessionTrackingControl instance based on params
        """
        return SessionTrackingControl(
            self._params['rhost'],
            '%s::%s' % (socket.getfqdn(), self._params['service']),
            SESSION_TRACKING_FORMAT_OID_USERNAME,
            self._params.get('ruser', self._params['username']),
        )


class PAMAuthcReq(PAMRequest):
    """
    handles PAM authc requests
    """
    rtype = PAM_REQ_AUTHC

    def _read_params(self) -> dict:
        params = dict(
            username=self.tios.read_string(),
            service=self.tios.read_string(),
            ruser=self.tios.read_string(),
            rhost=self.tios.read_string(),
            tty=self.tios.read_string(),
            password=self.tios.read_string(),
        )
        if 'rhost' in CFG.pam_authc_cache_attrs:
            params['rhost'] = self._get_rhost(params)
        return params

    def write(self, username, authc, authz, msg):
        """
        write result to PAM client
        """
        self.tios.write_int32(req.RES_BEGIN)
        self.tios.write_int32(authc)
        self.tios.write_string(username)
        self.tios.write_int32(authz)
        self.tios.write_string(msg)
        self.tios.write_int32(req.RES_END)

    def _cache_key(self):
        key_bytes = repr([
            (key, self._params[key])
            for key in sorted(self._params.keys())
            if key in CFG.pam_authc_cache_attrs
        ]).encode('utf-8')
        self._log(logging.DEBUG, '._cache_key() key_bytes = %r', key_bytes)
        chash = hashlib.sha512(AUTHC_CACHE_SALT)
        chash.update(key_bytes)
        return chash.digest()

    def process(self):
        """
        handle request, mainly do LDAP simple bind
        """
        user_name = self._params['username']
        if user_name not in PASSWD_NAME_MAP:
            raise ValueError('Invalid user name %r' % user_name)
        # initialize safe result vars
        pam_authc, pam_authz, pam_msg = (PAM_AUTH_ERR, PAM_PERM_DENIED, 'Internal error')
        if CFG.pam_authc_cache_ttl > 0:
            # lookup request result in cache
            cache_key = self._cache_key()
            try:
                pam_authc, pam_authz, pam_msg, etime = AUTHC_CACHE[cache_key]
            except KeyError:
                self._log(
                    logging.DEBUG,
                    'No cached PAM authc result => proceed with LDAP simple bind',
                )
            else:
                # check if cached result is still valid (within TTL)
                if etime >= time.time():
                    self._log(
                        logging.DEBUG,
                        'Return cached PAM authc result: %r',
                        (pam_authc, pam_authz, pam_msg),
                    )
                    self.write(user_name, pam_authc, pam_authz, pam_msg)
                    return
                self._log(
                    logging.DEBUG,
                    'Cached PAM authc result expired => proceed with LDAP simple bind',
                )
        ppolicy_ctrl = None
        # bind using the specified credentials
        uris = CFG.get_ldap_uris()
        if LDAP_CONN.current_ldap_uri is not None:
            # if currently connected then try the currently used LDAP server at first
            uris.append(LDAP_CONN.current_ldap_uri)
        self._log(logging.DEBUG, 'Will try simple bind on servers %r', uris)
        try:
            while True:
                ldap_uri = uris.pop()
                self._log(logging.DEBUG, 'Try connecting to %r', ldap_uri)
                try:
                    # open a separate connection
                    conn = aedir.AEDirObject(
                        ldap_uri,
                        trace_level=0,
                        retry_max=0,
                        timeout=CFG.timelimit,
                        cacert_filename=CFG.tls_cacertfile,
                        cache_ttl=0.0,
                    )
                    # do anon search to provoke connection failure
                    search_base = conn.search_base
                except ldap0.SERVER_DOWN:
                    if not uris:
                        raise
                else:
                    self._log(logging.DEBUG, 'Connected to %r', conn.uri)
                    break
            if user_name == CFG.aehost_vaccount_t[0]:
                self._log(logging.DEBUG, 'Use aehostd.conf binddn %r', CFG.binddn)
                user_dn = CFG.binddn
            else:
                self._log(
                    logging.DEBUG,
                    'Construct short user bind-DN from %r and %r',
                    user_name,
                    search_base,
                )
                user_dn = 'uid=%s,%s' % (user_name, search_base)
            self._log(logging.DEBUG, 'user_dn = %r', user_dn)
            bind_res = conn.simple_bind_s(
                user_dn,
                self._params['password'],
                req_ctrls=[
                    PasswordPolicyControl(),
                    self._session_tracking_control(),
                ],
            )
        except ldap0.INVALID_CREDENTIALS as ldap_err:
            self._log(logging.WARN, 'LDAP simple bind failed for %r: %s', user_dn, ldap_err)
            pam_authc, pam_msg = (PAM_AUTH_ERR, 'Wrong username or password')
        except ldap0.LDAPError as ldap_err:
            self._log(logging.WARN, 'LDAP error checking password for %r: %s', user_dn, ldap_err)
            pam_authc = PAM_AUTH_ERR
        else:
            self._log(logging.DEBUG, 'LDAP simple bind successful for %r on %r', user_dn, conn.uri)
            pam_authc = pam_authz = PAM_SUCCESS
            if user_name == CFG.aehost_vaccount_t[0]:
                pam_msg = 'Host password check ok'
                pam_authz = PAM_PERM_DENIED
                CFG.bindpwfile.write(self._params['password'].encode('utf-8'), mode=0o0640)
                refresh.USERSUPDATER_TASK.reset()
            else:
                pam_msg = 'User password check ok'
            # search password policy response control
            for ctrl in bind_res.ctrls:
                if ctrl.controlType == PasswordPolicyControl.controlType:
                    ppolicy_ctrl = ctrl
                    break
        if ppolicy_ctrl:
            # found a password policy control
            self._log(
                logging.DEBUG,
                'PasswordPolicyControl: error=%r, timeBeforeExpiration=%r, graceAuthNsRemaining=%r',
                ppolicy_ctrl.error,
                ppolicy_ctrl.timeBeforeExpiration,
                ppolicy_ctrl.graceAuthNsRemaining,
            )
            if ppolicy_ctrl.error == 0:
                # password is expired but still grace logins
                pam_authz, pam_msg = (PAM_AUTHTOK_EXPIRED, 'Password expired')
                if ppolicy_ctrl.graceAuthNsRemaining is not None:
                    pam_msg += ', %d grace logins left' % (
                        ppolicy_ctrl.graceAuthNsRemaining,
                    )
            elif ppolicy_ctrl.error is None and \
                ppolicy_ctrl.timeBeforeExpiration is not None:
                pam_msg = 'Password will expire in %d seconds' % (
                    ppolicy_ctrl.timeBeforeExpiration,
                )
        if pam_authc == PAM_SUCCESS and pam_authz == PAM_SUCCESS:
            pam_log_level = logging.DEBUG
        else:
            pam_log_level = logging.WARN
        self._log(
            pam_log_level,
            'PAM auth result for %r: authc=%d authz=%d msg=%r',
            user_name, pam_authc, pam_authz, pam_msg
        )
        if CFG.pam_authc_cache_ttl > 0:
            # store result in cache
            AUTHC_CACHE[self._cache_key()] = (
                pam_authc,
                pam_authz,
                pam_msg,
                time.time()+CFG.pam_authc_cache_ttl,
            )
        self.write(user_name, pam_authc, pam_authz, pam_msg)
        # end of handle_request()


class PAMAuthzReq(PAMRequest):
    """
    handles PAM authz requests
    """

    rtype = PAM_REQ_AUTHZ

    def _read_params(self) -> dict:
        params = dict(
            username=self.tios.read_string(),
            service=self.tios.read_string(),
            ruser=self.tios.read_string(),
            rhost=self.tios.read_string(),
            tty=self.tios.read_string(),
        )
        params['rhost'] = self._get_rhost(params)
        return params

    def write(self, authz, msg):
        """
        write result to PAM client
        """
        self.tios.write_int32(req.RES_BEGIN)
        self.tios.write_int32(authz)
        self.tios.write_string(msg)
        self.tios.write_int32(req.RES_END)

    def _check_authz_search(self):
        if not CFG.pam_authz_search:
            return
        # escape all params
        variables = dict((k, ldap0.filter.escape_str(v)) for k, v in self._params.items())
        variables.update(
            hostname=ldap0.filter.escape_str(socket.gethostname()),
            fqdn=ldap0.filter.escape_str(socket.getfqdn()),
            uid=variables['username'],
        )
        filter_tmpl = CFG.pam_authz_search
        if 'rhost' in variables and variables['rhost']:
            filter_tmpl = '(&%s(|(!(aeRemoteHost=*))(aeRemoteHost={rhost})))' % (filter_tmpl)
        ldap_filter = filter_tmpl.format(**variables)
        self._log(logging.DEBUG, 'check authz filter %r', ldap_filter)
        ldap_conn = LDAP_CONN.get_ldap_conn()
        ldap_conn.find_unique_entry(
            ldap_conn.search_base,
            filterstr=ldap_filter,
            attrlist=['1.1'],
            req_ctrls=[self._session_tracking_control()],
        )
        # end of _check_authz_search()

    def process(self):
        """
        handle request, mainly do LDAP authz search
        """
        user_name = self._params['username']
        if user_name not in PASSWD_NAME_MAP:
            self._log(logging.WARN, 'Invalid user name %r', user_name)
            self.write(PAM_PERM_DENIED, 'Invalid user name')
            return
        if user_name == CFG.aehost_vaccount_t[0]:
            self._log(logging.INFO, 'Reject login with host account %r', user_name)
            self.write(PAM_PERM_DENIED, 'Host account ok, but not authorized for login')
            return
        # check authorisation search
        try:
            self._check_authz_search()
        except ldap0.LDAPError as ldap_err:
            self._log(logging.WARNING, 'authz failed for %s: %s', user_name, ldap_err)
            self.write(PAM_PERM_DENIED, 'LDAP authz check failed')
        except (KeyError, ValueError, IndexError) as err:
            self._log(logging.WARNING, 'Value check failed for %s: %s', user_name, err)
            self.write(PAM_PERM_DENIED, 'LDAP authz check failed')
        else:
            self._log(logging.DEBUG, 'authz ok for %s', user_name)
            # all tests passed, return OK response
            self.write(PAM_SUCCESS, '')


class PAMPassModReq(PAMRequest):
    """
    handles PAM passmod requests
    """

    rtype = PAM_REQ_PWMOD

    def _read_params(self) -> dict:
        return dict(
            username=self.tios.read_string(),
            service=self.tios.read_string(),
            ruser=self.tios.read_string(),
            rhost=self.tios.read_string(),
            tty=self.tios.read_string(),
            asroot=self.tios.read_int32(),
            oldpassword=self.tios.read_string(),
            newpassword=self.tios.read_string(),
        )

    def write(self, res, msg):
        """
        write result to PAM client
        """
        self.tios.write_int32(req.RES_BEGIN)
        self.tios.write_int32(res)
        self.tios.write_string(msg)
        self.tios.write_int32(req.RES_END)

    def process(self):
        """
        handle request, just refuse password change
        """
        self.write(PAM_PERM_DENIED, CFG.pam_passmod_deny_msg)


class PAMSessOpenReq(PAMRequest):
    """
    handles PAM session open requests
    """

    rtype = PAM_REQ_SESS_O

    def _read_params(self) -> dict:
        return dict(
            username=self.tios.read_string(),
            service=self.tios.read_string(),
            ruser=self.tios.read_string(),
            rhost=self.tios.read_string(),
            tty=self.tios.read_string(),
        )

    def write(self, sessionid):
        """
        write result to PAM client
        """
        self.tios.write_int32(req.RES_BEGIN)
        self.tios.write_string(sessionid)
        self.tios.write_int32(req.RES_END)

    def process(self):
        """
        handle request, mainly return new generated session id
        """
        session_id = random_string(alphabet=SESSION_ID_ALPHABET, length=SESSION_ID_LENGTH)
        self._log(logging.DEBUG, 'New session ID: %s', session_id)
        self.write(session_id)


class PAMSessCloseReq(PAMRequest):
    """
    handles PAM session close requests
    """

    rtype = PAM_REQ_SESS_C

    def _read_params(self) -> dict:
        return dict(
            username=self.tios.read_string(),
            service=self.tios.read_string(),
            ruser=self.tios.read_string(),
            rhost=self.tios.read_string(),
            tty=self.tios.read_string(),
            session_id=self.tios.read_string(),
        )

    def write(self):
        """
        write result to PAM client
        """
        self.tios.write_int32(req.RES_BEGIN)
        self.tios.write_int32(req.RES_END)

    def process(self):
        """
        handle request, do nothing yet
        """
        self.write()
