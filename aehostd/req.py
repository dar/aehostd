# -*- coding: utf-8 -*-
"""
aehostd.req - base stuff for processing requests
"""

import logging
import inspect
import sys
from importlib import import_module


# The following constants have to match content of nslcd.h
PROTO_VERSION = 0x00000002
RES_BEGIN = 1
RES_END = 2


class Request:
    """
    Request handler class. Subclasses are expected to handle actual requests
    and should implement the following members:

      rtype - the request type handled by a class

      read_params() - a function that reads the request params of the
                          request stream
      write() - function that writes a single LDAP entry to the result stream

    """
    __slots__ = (
        'tios',
        'server',
        'peer',
        '_log_prefix',
        '_params',
    )
    rtype = None

    def __init__(self, tios, server, peer):
        self.tios = tios
        self.server = server
        self.peer = peer
        self._log_prefix = self._get_log_prefix()
        self._params = self._read_params()

    def _get_log_prefix(self):
        pid, uid, gid = self.peer
        return 'pid=%d uid=%d gid=%d %s' % (pid, uid, gid, self.__class__.__name__)

    def _get_peer_env(
            self,
            names=('SSH_CLIENT', 'SSH_CONNECTION', 'SSH_TTY'),
        ):
        """
        return dictionary of the peer's environment vars grabbed from /proc/<pid>
        """
        # FIX ME!
        # This currently only works on Linux because of access to /proc file-system.
        if sys.platform != 'linux':
            self._log(
                logging.DEBUG,
                'Platform is %r => skip reading peer env',
                sys.platform
            )
            return {}
        names = set(names or [])
        pid = self.peer[0]
        peer_env_filename = '/proc/%d/environ' % (pid,)
        try:
            with open(peer_env_filename, 'r') as env_file:
                env_str = env_file.read()
        except IOError as err:
            self._log(
                logging.DEBUG,
                'Error reading peer env from %s: %s',
                peer_env_filename,
                err,
            )
            return {}
        env = {}
        for line in env_str.split('\x00'):
            try:
                name, val = line.split('=', 1)
            except ValueError:
                continue
            if not names or name in names:
                env[name] = val
        self._log(logging.DEBUG, 'Retrieved peer env vars: %r', env)
        return env

    def _log(self, log_level, msg, *args, **kwargs):
        msg = ' '.join((self._log_prefix, msg))
        logging.log(log_level, msg, *args, **kwargs)

    def _read_params(self) -> dict:
        """
        Read and return the input params from the input stream
        """
        return dict()

    def get_results(self, params):
        """
        get results for params
        """
        return []

    def write(self, result):
        """
        send result to client
        just a place holder must be over-written by derived classes
        """
        raise RuntimeError(
            '%s.write() must not be directly used!' % (self.__class__.__name__,)
        )

    def process(self):
        """
        This method handles the request based on the params read
        with read_params().
        """
        res_count = 0
        for res in self.get_results(self._params):
            res_count += 1
            self._log(logging.DEBUG, 'res#%d: %r', res_count, res)
            self.tios.write_int32(RES_BEGIN)
            self.write(res)
        if not res_count:
            self._log(logging.DEBUG, 'no result')
        # write the final result code
        self.tios.write_int32(RES_END)

    def log_params(self, log_level):
        if self._params is not None:
            params = dict(self._params)
            for param in ('password', 'oldpassword', 'newpassword'):
                if params.get(param):
                    params[param] = '***'
        self._log(log_level, '(%r)', params)


def get_handlers(module_name):
    """
    Return a dictionary mapping request types to Request handler classes.
    """
    res = {}
    module = import_module(module_name)
    logging.debug('Inspecting module %s: %s', module_name, module)
    for _, cls in inspect.getmembers(module, inspect.isclass):
        if (
                issubclass(cls, Request)
                and hasattr(cls, 'rtype')
                and cls.rtype is not None
            ):
            res[cls.rtype] = cls
    logging.debug(
        'Registered %d request classes in module %s: %s',
        len(res),
        module_name,
        ', '.join([cls.__name__ for cls in res.values()]),
    )
    return res
