# -*- coding: utf-8 -*-
"""
aehostd.service - Unix domain socket server
"""

import sys
import os
import socket
import socketserver
import time
import logging
import logging.handlers
import struct
import argparse
import signal

from lockfile.pidlockfile import PIDLockFile
import daemon

from .__about__ import __version__
from .cfg import CFG
from . import req
from .tiostream import TIOStream
from .req import PROTO_VERSION


SO_PEERCRED_DICT = {
    'linux': (17, '3i'), # for Linux systems
}

# default umask used
UMASK_DEFAULT = 0o0022

# log format to use when logging to syslog
SYS_LOG_FORMAT = '%(levelname)s %(message)s'

# log format to use when logging to console
CONSOLE_LOG_FORMAT = '%(asctime)s %(levelname)s %(message)s'


def cli_args(script_name, service_desc):
    """
    CLI arguments
    """
    parser = argparse.ArgumentParser(
        prog=script_name,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description=service_desc,
    )
    parser.add_argument(
        '-f', '--config',
        dest='cfg_filename',
        default='/etc/aehostd.conf',
        help='configuration file name',
        required=False,
    )
    parser.add_argument(
        '-p', '--pid',
        dest='pidfile',
        default=os.path.join('/', 'var', 'run', 'aehostd', script_name)+'.pid',
        help='PID file name',
        required=False,
    )
    parser.add_argument(
        '-l', '--log-level',
        dest='log_level',
        default=None,
        help='log level',
        type=int,
        required=False,
    )
    parser.add_argument(
        '-n', '--no-fork',
        dest='no_fork',
        default=False,
        help='Do not fork or daemonise, run in the foreground.',
        action='store_true',
        required=False,
    )
    parser.add_argument(
        '-c', '--check',
        dest='check_only',
        default=False,
        help='Check whether demon is running.',
        action='store_true',
        required=False,
    )
    return parser.parse_args()
    # end of cli_args()


def init_logger(log_name, log_level, no_fork):
    """
    Returns a combined SysLogHandler/StreamHandler logging instance
    with formatters
    """
    if CFG.logsocket is None and no_fork:
        # send messages to stderr (console)
        log_format = CONSOLE_LOG_FORMAT
        log_handler = logging.StreamHandler()
    else:
        # send messages to syslog
        log_format = '{name}[{pid}] {fmt}'.format(
            name=log_name,
            pid=os.getpid(),
            fmt=SYS_LOG_FORMAT
        )
        log_handler = logging.handlers.SysLogHandler(
            address=CFG.logsocket or '/dev/log',
            facility=logging.handlers.SysLogHandler.LOG_DAEMON,
        )
    log_handler.setFormatter(logging.Formatter(fmt=log_format))
    the_logger = logging.getLogger()
    the_logger.addHandler(log_handler)
    if log_level is None:
        log_level = logging.INFO
    the_logger.setLevel(log_level)
    # end of init_logger()


def init_runtimedir(runtime_dir):
    """
    create run-time directory and set ownership and permissions
    """
    if os.getuid() != 0:
        logging.debug('Started as non-root user, leave run-time directory %r as is', runtime_dir)
        return
    try:
        if not os.path.exists(runtime_dir):
            logging.debug('Creating run-time directory %r', runtime_dir)
            os.mkdir(runtime_dir)
        logging.debug('Set permissions and ownership of run-time directory %r', runtime_dir)
        os.chown(runtime_dir, CFG.uid, CFG.gid)
        os.chmod(runtime_dir, 0o755)
    except (IOError, OSError) as err:
        logging.warning(
            'Failed setting permissions and ownership of run-time directory %r: %s',
            runtime_dir,
            err,
        )
    return # endof init_runtimedir()


def init_service(log_name, service_desc, service_uid=None, service_gid=None):
    """
    initialize the service instance
    """
    script_name = os.path.basename(sys.argv[0])
    # extract command-line arguments
    args = cli_args(script_name, service_desc)
    # read configuration file
    CFG.read_config(args.cfg_filename)
    init_logger(log_name, args.log_level, args.no_fork)
    logging.info(
        'Starting %s %s [%d] reading config %s',
        script_name, __version__,
        os.getpid(),
        args.cfg_filename,
    )
    # log config currently effective options
    for cfg_key in sorted(CFG.__slots__):
        logging.debug('%s = %r', cfg_key, getattr(CFG, cfg_key))
    # clean the environment
    os.environ.clear()
    os.environ['HOME'] = '/'
    os.environ['TMPDIR'] = os.environ['TMP'] = '/tmp'
    os.environ['LDAPNOINIT'] = '1'
    # set log level
    if args.log_level is None:
        logging.getLogger().setLevel(CFG.loglevel)
    # set a default umask for the pidfile and socket
    os.umask(UMASK_DEFAULT)
    # see if someone already locked the pidfile
    pidfile = PIDLockFile(args.pidfile)
    runtime_dir = os.path.dirname(CFG.socketpath)
    init_runtimedir(runtime_dir)
    # see if --check option was given
    if args.check_only:
        if pidfile.is_locked():
            logging.debug('pidfile (%s) is locked', args.pidfile)
            sys.exit(0)
        else:
            logging.debug('pidfile (%s) is not locked', args.pidfile)
            sys.exit(1)
    # normal check for pidfile locked
    if pidfile.is_locked():
        logging.error(
            'daemon may already be active, cannot acquire lock (%s)',
            args.pidfile,
        )
        sys.exit(1)
    # daemonize
    if args.no_fork:
        ctx = pidfile
    else:
        if service_uid is None:
            demon_uid = CFG.uid
        else:
            demon_uid = service_uid
        if service_gid is None:
            demon_gid = CFG.gid
        else:
            demon_gid = service_gid
        ctx = daemon.DaemonContext(
            pidfile=pidfile,
            umask=UMASK_DEFAULT,
            uid=demon_uid,
            gid=demon_gid,
            signal_map={
                signal.SIGTERM: u'terminate',
                signal.SIGINT: u'terminate',
                signal.SIGPIPE: None,
            }
        )
    return script_name, ctx # end of init_service()


class TIOStreamRequestHandler(socketserver.BaseRequestHandler):
    """
    handling a TIO stream request for NSS/PAM
    """

    def _get_peer_cred(self):
        try:
            so_num, struct_fmt = SO_PEERCRED_DICT[sys.platform]
        except KeyError:
            return None, None, None
        peer_creds_struct = self.request.getsockopt(
            socket.SOL_SOCKET,
            so_num,
            struct.calcsize(struct_fmt)
        )
        pid, uid, gid = struct.unpack(struct_fmt, peer_creds_struct)
        return pid, uid, gid  # _get_peer_cred()

    def handle(self):
        """
        handle a single request
        """
        start_time = time.time()
        self.server._req_counter_all += 1
        req_file = self.request.makefile(mode='rwb')
        # create a stream object
        tios = TIOStream(req_file)
        # read request
        nslcd_version = tios.read_int32()
        if nslcd_version != req.PROTO_VERSION:
            logging.error(
                'Wrong protocol version: Expected %r but got %r',
                req.PROTO_VERSION,
                nslcd_version
            )
            return
        req_type = tios.read_int32()
        logging.debug('Incoming request on %s', self.request.getsockname())
        try:
            handler_class = self.server._reqh[req_type]
        except KeyError:
            logging.error('No handler for req_type 0x%08x', req_type)
            self.server._invalid_requests += 1
            return
        self.server._req_counter[req_type] += 1
        handler = None
        try:
            handler = handler_class(
                tios,
                self.server,
                self._get_peer_cred(),
            )
            handler.log_params(logging.DEBUG)
            handler.tios.write_int32(PROTO_VERSION)
            handler.tios.write_int32(handler.rtype)
            handler.process()
        except (KeyboardInterrupt, SystemExit) as exit_err:
            logging.debug(
                'Received %s exception in %s => re-raise',
                exit_err,
                self.__class__.__name__,
            )
            # simply re-raise the exit exception
            raise
        except Exception:
            if handler is not None:
                handler.log_params(logging.ERROR)
            logging.error('Unhandled exception during processing request:', exc_info=True)
        req_time = 1000 * (time.time() - start_time)
        self.server._avg_response_time = (self.server._avg_response_time*30 + req_time) / 31
        self.server._max_response_time = max(self.server._max_response_time, req_time)
        # end of TIOStreamRequestHandler.handle()


class NSSPAMServer(socketserver.UnixStreamServer):
    """
    the NSS/PAM socket server
    """

    def __init__(
            self,
            server_address,
            bind_and_activate=True
        ):
        socketserver.UnixStreamServer.__init__(
            self,
            server_address,
            TIOStreamRequestHandler,
            bind_and_activate,
        )
        self._start_time = time.time()
        self._last_access_time = 0
        self._req_counter_all = 0
        self._invalid_requests = 0
        self._bytes_sent = 0
        self._bytes_received = 0
        self._avg_response_time = 0
        self._max_response_time = 0
        # initialize a map of request handler classes
        self._reqh = {}
        self._reqh.update(req.get_handlers('aehostd.config'))
        self._reqh.update(req.get_handlers('aehostd.group'))
        self._reqh.update(req.get_handlers('aehostd.hosts'))
        self._reqh.update(req.get_handlers('aehostd.passwd'))
        self._reqh.update(req.get_handlers('aehostd.pam'))
        # initialize a map of per-request-type request counters
        self._req_counter = {}.fromkeys(self._reqh.keys(), 0)

    def get_monitor_data(self):
        """
        returns all monitoring data as
        """
        return dict(
            req_count=self._req_counter_all,
            req_err=self._invalid_requests,
            avg_response_time=self._avg_response_time,
            max_response_time=self._max_response_time,
        )

    def server_bind(self):
        """Override server_bind to set socket options."""
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.settimeout(CFG.sockettimeout)
        try:
            os.unlink(self.server_address)
        except OSError:
            if os.path.exists(self.server_address):
                raise
        socketserver.UnixStreamServer.server_bind(self)
        os.chmod(self.server_address, int(CFG.socketperms, 8))
        logging.debug(
            '%s now accepting connections on %r',
            self.__class__.__name__,
            self.server_address,
        )
        # end of server_bind()
