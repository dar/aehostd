#!/bin/sh

python2 setup.py clean --all
rm -r MANIFEST .coverage dist/aehostd* build/* *.egg-info .tox docs/.build/*
rm aehostd/*.py? aehostd/*/*.py? tests/*.py? *.py?
find -name "*.py?" -delete
find -name __pycache__ | xargs -n1 -iname rm -r name
rm -r slapdtest-[0-9]*
