# -*- coding: utf-8 -*-
"""
package/install aehostd
"""

import sys
import os
from setuptools import setup, find_packages

PYPI_NAME = 'aehostd'

BASEDIR = os.path.dirname(os.path.realpath(__file__))

sys.path.insert(0, os.path.join(BASEDIR, PYPI_NAME))
import __about__

setup(
    name=PYPI_NAME,
    license=__about__.__license__,
    version=__about__.__version__,
    description='AE-DIR host demon',
    author=__about__.__author__,
    author_email=__about__.__mail__,
    maintainer=__about__.__author__,
    maintainer_email=__about__.__mail__,
    url='https://ae-dir.com/aehostd.html',
    download_url='https://pypi.org/project/%s/#files' % (PYPI_NAME),
    keywords=['LDAP', 'LDAPv3', 'OpenLDAP', 'AE-DIR', 'Æ-DIR'],
    packages=find_packages(exclude=['tests']),
    package_dir={'': '.'},
    test_suite='tests',
    python_requires='>=3.6',
    include_package_data=True,
    data_files=[],
    install_requires=[
        'setuptools',
        'lockfile',
        'python-daemon',
        'ldap0>=0.6.7',
        'aedir>=0.6.0',
    ],
    zip_safe=False,
    entry_points={
        'console_scripts': [
            # the main NSS/PAM service
            '{0}={0}.srv:main'.format(PYPI_NAME),
            # the privileged helper service
            '{0}-ph={0}.priv:main'.format(PYPI_NAME),
        ],
    }
)
